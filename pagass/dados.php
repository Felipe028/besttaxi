<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="img/2a.png">
<title>Best Táxi</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<!--Reveal modal-->
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<script type="text/javascript" src="js/cidades-estados-v0.2.js"></script> 
<script type="text/javascript">
    window.onload = function() {
        new dgCidadesEstados( 
            document.getElementById('estado'), 
            document.getElementById('cidade'), 
            true
        );
    }
</script>
<link rel="stylesheet" href="css/reveal.css">
<!--Fim Reveal modal-->
</head>

<body>
<p style="margin: 5px 0px 0px 40px; font-size:120%; color: #555555;">Dados</p>
<a href="#" style="float: right; margin: -18px 2px 0px 0px;" class="novo" data-reveal-id="editar" data-animation="fade">Editar dados</a>
<?php
$sql = mysql_query("SELECT * FROM associacao WHERE idassociacao = '".$_SESSION['idassociacao']."'");
$ln = mysql_fetch_array($sql);
?>
<div style="margin: 22px 0px 0px 30px; width: 600px; height: 272px; background: #eeeeee; border: 1px solid #cfcfcf;  border-radius:6px;">

<div style="float: left; width: 170px; text-align: right; ">
<p style="margin: 20px 0px 8px 0px;"><strong>Nome:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Endereço:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Complemento:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Bairro:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Cep:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Cidade:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>CNPJ:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Telefone:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>E-mail:</strong></p>
</div>

<div style="float: left; width: 424px; text-align: left; margin: 0px 0px 0px 6px;">
<p style="margin: 20px 0px 8px 0px;"><?php echo $ln['nome'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo "Rua ".$ln['rua']."; Nº ".$ln['numero']."";?></p>
<p style="margin: 0px 0px 8px 0px;"><?php if(empty($ln['complemento'])){echo "-";}else{echo $ln['complemento'];}?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['bairro_ass'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['cep'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo "".$ln['cidade_ass']."-".$ln['estado_ass']."";?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['cnpj'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo "(".$ln['ddd_telefone_ass'].") ".$ln['telefone']."";?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['email'];?></p>
</div>
</div>


		<!--FILIAR-->
		<div id="editar" class="reveal-modal">
		<form method="post" action="">
		<div style=" width: 520px; text-align: center;">
		<p style="font-size:120%; color: #555555; margin: 0px 0px 8px 0px;">Alterar Dados</p>

	Nome: <input type="text" name="nome" placeholder="NOME" style="width: 438px; margin: 0px 0px 10px 2px;" value="<?php echo $ln['nome'];?>"/><br/>
	<!--input para receber somente "numeros"-->
    CNPJ: <input type="text" name="cnpj" placeholder="CNPJ" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 100px; margin: 0px 20px 10px 0px;" maxlength="14" value="<?php echo $ln['cnpj'];?>"/>
    <!--FIM input-->
	Telefone: <!--input para receber somente "numeros"-->
    <input type="text" name="ddd" placeholder="DDD" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 30px; text-align: center" maxlength="2" value="<?php echo $ln['ddd_telefone_ass'];?>"/>
    <!--FIM input-->
	<!--input para receber somente "numeros"-->
     - <input type="text" name="telefone" placeholder="TELEFONE" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 100px; margin: 0px 88px 10px 0px;" maxlength="9" value="<?php echo $ln['telefone'];?>"/><br/>
    <!--FIM input-->
	Rua: <input type="text" name="rua" placeholder="RUA" style="width: 344px; margin: 0px 8px 10px 16px;" value="<?php echo $ln['rua'];?>"/>
	<!--input para receber somente "numeros"-->
    Nº: <input type="text" name="numero" placeholder="Nº" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 53px;" maxlength="6" value="<?php echo $ln['numero'];?>"/><br/>
    <!--FIM input-->
	Complemento: <input type="text" name="complemento" placeholder="COMPLEMENTO" style="width: 390px; margin: 0px 0px 10px 0px;" value="<?php echo $ln['complemento'];?>"/><br/>
	Bairro: <input type="text" name="bairro" placeholder="BAIRRO" style="width: 315px; margin: 0px 6px 10px 3px;" value="<?php echo $ln['bairro_ass'];?>"/>
	<!--input para receber somente "numeros"-->
    CEP: <input type="text" name="cep" placeholder="CEP" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 70px;" maxlength="8" value="<?php echo $ln['cep'];?>"/><br/>
    <!--FIM input-->
	
	<fieldset style="border: none; margin: 0px 0px 2px -12px;">
		<label>Estado</label>: <select id="estado" name="estado"></select> 
		<label>Cidade</label>: <select id="cidade" name="cidade"></select> 
	</fieldset>
	
	E-mail: <input type="text" name="email" placeholder="E-MAIL" style="width: 232px; margin: 0px 0px 20px 0px;" value="<?php echo $ln['email'];?>"/>
	Senha: <input type="text" name="senha" placeholder="SENHA" style="width: 150px;" value="<?php echo $ln['senha'];?>"/>
		
		
		<input type="hidden" name="acao" value="comentar"/>
		<input type="submit" value="Concluir" class="form-cad" style="width: 100px; height: 35px; margin: 0px 0px 0px 390px; border-radius:5px;"/>
		</div>
		<a href="" class="close-reveal-modal"> x</a>
		</form>
		</div>
        <!--fim filiar-->		
<!--PHP Alterar-->
<?php
if(isset($_POST['acao']) && $_POST['acao'] == 'comentar'){
if(empty($_POST['nome']) || empty($_POST['cnpj']) || empty($_POST['rua']) || empty($_POST['numero']) || empty($_POST['bairro']) || empty($_POST['cidade']) || empty($_POST['estado']) || empty($_POST['cep']) || empty($_POST['ddd']) || empty($_POST['telefone']) || empty($_POST['email']) || empty($_POST['senha']) || strlen($_POST['cnpj']) != 14 || strlen($_POST['cep']) != 8 || strlen($_POST['ddd']) != 2 || strlen($_POST['telefone']) < 8){
      echo '<script> alert("Preencha corretamente todos os campos")</script>';
	  
if(strlen($_POST['cnpj']) != 14){echo "<script> alert( 'CNPJ inválido' ); location = 'minha_conta_.php?p=dado'; </script>";}
if(strlen($_POST['cep']) != 8){echo "<script> alert( 'CEP inválido' ); location = 'minha_conta_.php?p=dado'; </script>";}
if(strlen($_POST['ddd']) != 2){echo "<script> alert( 'DDD inválido' ); location = 'minha_conta_.php?p=dado'; </script>";}
if(strlen($_POST['telefone']) < 8){echo "<script> alert( 'Telefone inválido' ); location = 'minha_conta_.php?p=dado'; </script>";}	  
}


else{
$sql = mysql_query("UPDATE associacao SET nome = '".$_POST['nome']."', cnpj = '".$_POST['cnpj']."', rua = '".$_POST['rua']."', numero = '".$_POST['numero']."', complemento = '".$_POST['complemento']."', bairro_ass = '".$_POST['bairro']."', cidade_ass = '".$_POST['cidade']."', estado_ass = '".$_POST['estado']."', cep = '".$_POST['cep']."', ddd_telefone_ass = '".$_POST['ddd']."', telefone = '".$_POST['telefone']."', email = '".$_POST['email']."', senha = '".$_POST['senha']."' WHERE idassociacao = '".$_SESSION['idassociacao']."'");

echo "<script> location = 'minha_conta_.php?p=dado'; </script>";
}
}
?>
<!--FIM PHP Alterar-->		

</body>
</html>