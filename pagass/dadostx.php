<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="img/2a.png">
<title>Best Táxi</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<!--Reveal modal-->
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<script type="text/javascript" src="js/cidades-estados-v0.2.js"></script> 
<link rel="stylesheet" href="css/reveal.css">
<!--Fim Reveal modal-->
</head>

<body>
<p style="margin: 5px 0px 0px 40px; font-size:120%; color: #555555;">Dados</p>

<div style="float: right; margin: -18px 2px 0px 0px;">
<form method="post" action="" style="float: right;">
<input type="text" name="recl" style="display: none;" value="<?php echo $_POST['recl'];?>"/>
<input type="hidden" name="acao2" value="excluir"/>
<input type="submit" value="Rejeitar Solicitação" class="excl" style="width: 140px; height: 28px; border-radius:5px;"/>
</form>

<form method="post" action="" style="float: right; margin: 0px 10px 0px 0px;">
<input type="text" name="recl" style="display: none;" value="<?php echo $_POST['recl'];?>"/>
<input type="hidden" name="acao1" value="confimar"/>
<input type="submit" value="Confirmar Solicitação" class="confirmar" style="width: 140px; height: 28px; border-radius:5px;"/>
</form>
</div>

<div style="background: #ffffff; border-bottom: 2px solid #d7d6d6; border-radius:6px; margin: 0px 0px 0px 40px; width:760px; height:350px; text-align: center;">

<p style="margin: 20px auto 6px auto; font-size:120%; color: #555555;">Taxista</p>
<div style="float: left; width: 378px; text-align: right; ">
<p style="margin: 5px 0px 8px 0px;"><strong>Nome:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Endereço:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Complemento:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Bairro:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Cep:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Cidade:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>RG:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>CPF:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Data/Nascimento:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>CNH:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Telefone:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>E-mail:</strong></p>
</div>

<?php $sqlTax = mysql_query("SELECT * FROM taxista WHERE idtaxista = '".$_POST['recl']."'");
$ln = mysql_fetch_array($sqlTax); ?>
<div style="float: left; width: 370px; text-align: left; margin: 0px 0px 0px 6px;">
<p style="margin: 5px 0px 8px 0px;"><?php echo $ln['nome'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo "Rua ".$ln['rua']."; Nº ".$ln['numero']."";?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['complemento'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['bairro'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['cep'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo "".$ln['cidade_taxi']."-".$ln['estado_taxi']."";?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['rg'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['cpf'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo date("d-m-Y", strtotime($ln['data_nasc']));?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['cnh'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo "(".$ln['ddd'].") ".$ln['telefone']."";?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['email'];?></p>
</div>
</div>


<div style="margin: 20px 0px 0px 40px; background: #ffffff; border-bottom: 2px solid #d7d6d6; border-radius:6px; width:760px; text-align: center;">
<p style="margin: 0px auto 0px auto; font-size:120%; color: #555555;">Veículos</p>
<table style="margin: 10px auto 0px auto;">
    <tbody>
	    <tr class="tabela-new-venda">
		    <th style="width: 180px;">Modelo</th>
			<th style="width: 200px;">Marca</th>
			<th style="width: 80px;">Ano</th>
			<th style="width: 150px;">Cor</th>
			<th style="width: 100px;">Placa</th>
		</tr>
		
		<?php
		$sqlCar = mysql_query("SELECT * FROM carro_has_taxista WHERE taxista_idtaxista = '".$_POST['recl']."'");
		while($lnCar = mysql_fetch_array($sqlCar)){
			$sqlCar2 = mysql_query("SELECT * FROM carro WHERE idcarro = '".$lnCar['carro_idcarro']."'");
			$lnCar2 = mysql_fetch_array($sqlCar2);
			$sqlMar = mysql_query("SELECT * FROM marca WHERE idmarca = '".$lnCar2['marca_idmarca']."'");
			$lnMar = mysql_fetch_array($sqlMar);
			?>
		<tr>
		    <td style="text-align: center;"><?echo $lnCar2['nome'];?></td>
			<td style="text-align: center;"><?echo $lnMar['nome'];?></td>
			<td style="text-align: center;"><?echo $lnCar['ano'];?></td>
			<td style="text-align: center;"><?echo $lnCar['cor'];?></td>
			<td style="text-align: center;"><?echo $lnCar['placa'];?></td>
		</tr>
<?php } ?>
	</tbody>
</table>
</div>
	

<!--PHP Alterar-->
<?php
if(isset($_POST['acao1']) && $_POST['acao1'] == 'confimar'){
$sql = mysql_query("UPDATE taxista SET status_ass = '1' WHERE idtaxista = '".$_POST['recl']."' ");

echo "<script> location = 'minha_conta_.php?p=fili'; </script>";
}
if(isset($_POST['acao2']) && $_POST['acao2'] == 'excluir'){
$sql = mysql_query("UPDATE taxista SET status_ass = '4' WHERE idtaxista = '".$_POST['recl']."'");
echo "<script> location = 'minha_conta_.php?p=fili'; </script>";
}
?>
<!--FIM PHP Alterar-->		

</body>
</html>