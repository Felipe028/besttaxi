<?php
session_start();
if($_SESSION['tipo'] != 0){
	echo "<script>location = 'Logout.php';</script>";
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="img/2a.png">
<title>Best Táxi</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<!--Reveal modal-->
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<link rel="stylesheet" href="css/reveal.css">
<!--Fim Reveal modal-->
</head>

<body>
<div id="topo" style="background: #21c16a; height: 50px; width: 100%; padding: 7px 0px 0px 16px; margin: -8px 0px 0px -8px;">
<img src="img/2a.png" style="height:50px; width:50px; margin: -4px 0px 0px 14px;"/>
<p style="font-size:300%; font-family: Gabriola; position: absolute; top: 0px; margin: -10px 0px 0px 74px; color: #0000ff;">Best Táxi</p>
<a href="Logout.php" class="sair_m_c" style="float: right; padding: 18px 18px 17px 18px; font-size:120%; margin: -7px 20px 0px 0px; text-decoration:none;">Sair</a>
<a href="Index.php?p=home" class="sair_m_c" style="float: right; padding: 18px 12px 17px 12px; font-size:120%; margin: -7px 0px 0px 0px; text-decoration:none;">Menu</a>
</div>


<div style=" width: 170px; min-height: 96px; float: left; margin: 20px 0px 0px 24px; text-align: left; border-bottom: 2px solid #d7d6d6; -moz-border-radius:6px; -webkit-border-radius:6px;">
<p style="margin: 20px 0px 19px 4px;"><a href="minhaconta.php?p=d" style="padding: 10px 71px 10px 9px;" class="menu_lat">Meus Dados</a></p>
<p style="margin: 0px 0px 19px 4px;"><a href="minhaconta.php?p=r" style="padding: 10px 67px 10px 9px;" class="menu_lat">Reclamações</a></p>
</div>


<div style=" min-width: 500px; min-height: 500px; float: left; margin: 30px 0px 0px 0px;">
<?php
        include "conexao.php";
        include "pagcli/pagina.php";
?>
</div>


</body>
</html>