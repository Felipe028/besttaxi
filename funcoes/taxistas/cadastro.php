<?php
require("../../conexao.php");
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="../../img/2a.png">
<title>Best Táxi</title>
<link href="../../css/estilo.css" rel="stylesheet" type="text/css">
<!--Reveal modal-->
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<script type="text/javascript" src="../../js/cidades-estados-v0.2.js"></script> 
<script type="text/javascript">
    window.onload = function() {
        new dgCidadesEstados( 
            document.getElementById('estado'), 
            document.getElementById('cidade'), 
            true
        );
    }
</script>
<link rel="stylesheet" href="css/reveal.css">
<!--Fim Reveal modal-->
</head>

<body>
<div id="topo" style="background: #21c16a; height: 50px; width: 100%; padding: 7px 0px 0px 16px; margin: -8px 0px 90px -8px;">
<img src="../../img/2a.png" style="height:50px; width:50px; margin: -4px 0px 0px 14px;"/>
<p style="font-size:300%; font-family: Gabriola; position: absolute; top: 0px; margin: -10px 0px 0px 74px; color: #0000ff;">Best Táxi</p>
</div>

<div style="background: #eeeeee; width: 600px; height: 412px; margin: auto; border: 2px solid #c9c9c9; -moz-border-radius:6px; -webkit-border-radius:6px;">

	
	<h3 style="text-align:center; margin: 10px 0px 15px 0px;">Preecha o formulário abaixo com os seus dados</h3>	
	<form method="post" action="">
	<div style="text-align: left; width: 430 px; margin: auto; padding: 0px 0px 0px 50px;">
	
	Nome: <input type="text" name="nome" placeholder="NOME" style="width: 438px; margin: 0px 0px 10px 2px;"/><br/>
	<!--input para receber somente "numeros"-->
	<!--input para receber somente "numeros"-->
    CNH: <input type="text" name="cnh" placeholder="CNH" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 100px; margin: 0px 37px 10px 8px;" maxlength="10"/>
    <!--FIM input-->
    RG: <input type="text" name="rg" placeholder="RG" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 80px; margin: 0px 37px 10px 0px;" maxlength="8"/>
    <!--FIM input-->
	<!--input para receber somente "numeros"-->
    CPF: <input type="text" name="cpf" placeholder="CPF" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 100px; margin: 0px 0px 10px 0px;" maxlength="11"/><br/>
    <!--FIM input-->
	Rua: <input type="text" name="rua" placeholder="RUA" style="width: 344px; margin: 0px 8px 10px 16px;"/>
	<!--input para receber somente "numeros"-->
    Nº: <input type="text" name="numero" placeholder="Nº" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 53px;" maxlength="6"/><br/>
    <!--FIM input-->
	Complemento: <input type="text" name="complemento" placeholder="COMPLEMENTO" style="width: 390px; margin: 0px 0px 10px 0px;"/><br/>
	Bairro: <input type="text" name="bairro" placeholder="BAIRRO" style="width: 315px; margin: 0px 6px 10px 3px;"/>
	<!--input para receber somente "numeros"-->
    CEP: <input type="text" name="cep" placeholder="CEP" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 70px;" maxlength="8"/><br/>
    <!--FIM input-->
	
	<fieldset style="border: none; margin: 0px 0px 2px -12px;">
		<label>Estado</label>: <select id="estado" name="estado"></select> 
		<label>Cidade</label>: <select id="cidade" name="cidade"></select> 
	</fieldset>

	Selecione a associação a qual você pertence:
	<select name="associacao" style="width: 494px; height: 22px; margin: 0px 42px 10px 0px;">
				<option value="0" select></option>
				<?php
				$sqlForn = mysql_query("SELECT * FROM associacao order by nome ASC");
				while ($dadosForn = mysql_fetch_array($sqlForn)){ ?>
					<option value="<?php echo $dadosForn['idassociacao'];?>"><?php echo $dadosForn['nome'];?></option>
				<?php } ?>
	</select><br/>
	
	Data de nascimento: <input type="date" name="nascimento" style="width: 124px; margin: 2px 11px 10px 0px;"/>
	Telefone: <!--input para receber somente "numeros"-->
    <input type="text" name="ddd" placeholder="DDD" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 30px; text-align: center" maxlength="2"/>
    <!--FIM input-->
	<!--input para receber somente "numeros"-->
     - <input type="text" name="telefone" placeholder="TELEFONE" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 100px; text-align: center;" maxlength="9"/><br/>
    <!--FIM input-->
	E-mail: <input type="text" name="email" placeholder="E-MAIL" style="width: 232px; margin: 0px 0px 20px 0px;"/>
	Senha: <input type="text" name="senha" placeholder="SENHA" style="width: 150px;"/>
	
	    <input type="hidden" name="acao" value="comentar"/>
		<input type="submit" value="Cadastrar" class="form-cad" style="width: 100px; height: 35px;  margin: 0px 0px 0px 288px; -moz-border-radius:5px; -webkit-border-radius:5px;"/>
		<input type="reset" value="Limpar" class="form-cad" style="width: 100px; height: 35px; -moz-border-radius:5px; -webkit-border-radius:5px;"/>
	</div>
	</form>
	


</div>

<?php
include "../../conexao.php";
if(isset($_POST['acao']) && $_POST['acao'] == 'comentar'){

$sql = mysql_query("INSERT INTO taxista (associacao_idassociacao, nome, rua, numero, complemento, bairro, cidade_taxi, estado_taxi, cep, data_nasc, rg, cpf, cnh, ddd, telefone, email, senha, status_viagem, status_ass, tipo) values ('".$_POST['associacao']."', '".$_POST['nome']."', '".$_POST['rua']."', '".$_POST['numero']."', '".$_POST['complemento']."', '".$_POST['bairro']."', '".$_POST['cidade']."', '".$_POST['estado']."', '".$_POST['cep']."', '".$_POST['nascimento']."', '".$_POST['rg']."', '".$_POST['cpf']."', '".$_POST['cnh']."', '".$_POST['ddd']."', '".$_POST['telefone']."', '".$_POST['email']."', '".$_POST['senha']."', '0', '0', '1')");

echo "<script> alert( 'Solicitação de cadastro enviada! \\nAguarde a confirmação do seu cadastro!' ); location = '../../Index.php?p=home'; </script>";
}
?>

</body>
</html>