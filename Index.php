<?php
session_start();
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="img/2a.png">
<title>Best Táxi</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<!--Reveal modal-->
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<script type="text/javascript" src="js/cidades-estados-v0.2.js"></script>
<script type="text/javascript" src="js/cidades-estados-v0.1.js"></script> 
<script type="text/javascript">
    window.onload = function() {
        new dgCidadesEstados( 
            document.getElementById('estado'), 
            document.getElementById('cidade'), 
            true
        );

        new dgCidadesEstados1( 
            document.getElementById('estado1'), 
            document.getElementById('cidade1'), 
            true
        );		
    }
</script>
<link rel="stylesheet" href="css/reveal.css">
<!--Fim Reveal modal-->
</head>

<body>

<?php if(!isset($_SESSION["email"]) || !isset($_SESSION["senha"])){ ?>
<div style="margin: -8px 0px 0px 0px;">
<div style="width: 1000px; margin:0 auto; text-align: right; ">
<a href="#" data-reveal-id="cadastro" data-animation="fade" class="novo">Cadastre-se</a> | 
<a href="Login.php" class="novo">Entre</a>
</div>
</div>

<?php }else{ ?>
<div style="margin: -8px 0px 0px 0px;">
<div style="width: 1000px; margin:0 auto; text-align: right; ">
<?php echo $_SESSION['email'];?> | 
<?php if($_SESSION['tipo'] == 0) { ?> <a href="minhaconta.php" class="novo">Minha Conta</a> | <?php } ?>
<?php if($_SESSION['tipo'] == 1) { ?> <a href="minha_conta.php" class="novo">Minha Conta</a> | <?php } ?>
<?php if($_SESSION['tipo'] == 2) { ?> <a href="minha_conta_.php" class="novo">Minha Conta</a> | <?php } ?>
<a href="Logout.php" class="novo">Sair</a>
</div>
</div>
<?php } ?>

<div style="width: 1000px; height: 140px; margin:0 auto; text-align: right; background:; text-align: left;">
<img src="img/2a.png" style="height:150px; width:150px; margin: 0px 0px 0px 20px;"/>
<p style="font-size:1000%; font-family: Gabriola; position: absolute; top: 0px; width: 600px; height: 110px; margin: -40px 0px 0px 200px; color: #0000ff;">Best Táxi</p>
</div>
<br/>

<div class="topo">
        <ul>
			<li><a  href="Index.php?p=home">Home</a></li>
			<li><a  href="Index.php?p=viag">Viagens</a></li>
			<li><a  href="Index.php?p=taxi">Táxis</a></li>
			<li><a href="Index.php?p=asso">Associações</a></li>
			<li><a href="Index.php?p=fale">Fale Conosco</a></li>
		</ul>
</div>



<div style="width: 1000px; min-height: 440px; margin: auto; text-align: center; background: #f0f2ea; padding: 1px 0px 0px 0px;">
<?php
        include "conexao.php";
        include "pagina.php";
?>
</div>

		<!--CADASTRO-->
		<div id="cadastro" class="reveal-modal">
		<div style=" width: 370px; margin: auto; text-align: center;">
		<p>Para efetuar seu cadastro em nosso sistema, escolha abaixo a categoria de usurário que você deseja cadastrar.</p>
		<a href="funcoes/clientes/cadastro.php" class="escolha" style="padding: 8px 30px 8px 30px;">Cliente</a>
		<a href="funcoes/taxistas/cadastro.php" class="escolha" style="padding: 8px 30px 8px 30px;">Taxista</a>
		<a href="funcoes/associacoes/cadastro.php" class="escolha" style="padding: 8px 17px 8px 16px;">Associação</a>
		</div>
				<a href="" class="close-reveal-modal"> x</a>
		</div>
        <!--fim cadastro-->			
</body>
</html>