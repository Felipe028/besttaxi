<?php include("../conexao.php"); ?>
<?php
session_start();
if($_SESSION['tipo'] != 1){
	echo "<script>location = 'Logout.php';</script>";
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="img/2a.png">
<title>Best Táxi</title>
<link href="../css/estilo.css" rel="stylesheet" type="text/css">
<!--Reveal modal-->
<script type="text/javascript" src="../js/jquery-1.6.min.js"></script>

<script type="text/javascript" src="../js/jquery-1.8.2.min.js"></script> 
    <script type="text/javascript">
    $(document).ready(function(){
         
        $("#marca").change(function(){
            marca = $(this).attr('value');
            $.ajax({
                type: "post",
                url: "get_modelos.php",
                data: "marca="+marca+"",
                async: false,
                cache: false,
                datatype: "text",
                beforeSend: function(){
                    $("#modelo").html('<option value="" selected="selected">Aguarde...</option>');
                    $("#tipo").html('');
                },
                success: function(response){ 
                    x = response.split(",");
                    modelo = new Array();
                    for(i = 0; i < x.length; i++){
                        modelo[i] = x[i].split("|");
                    }
                    if(modelo.length > 0){
                        $("#modelo").html('').append('<option selected="selected">Selecione...</option>');
                        for(i = 0; i < modelo.length; i++) {
                            $("#modelo").append('<option value='+modelo[i][1]+'>'+modelo[i][0]+'</option>');
                        }   
                    }
                },
                error: function(){
                 
                }
            });
        });
    });
    </script>
	
<link rel="stylesheet" href="css/reveal.css">
<!--Fim Reveal modal-->
</head>
<body>
<div id="topo" style="background: #21c16a; height: 50px; width: 100%; padding: 7px 0px 0px 16px; margin: -8px 0px 0px -8px;">
<img src="../img/2a.png" style="height:50px; width:50px; margin: -4px 0px 0px 14px;"/>
<p style="font-size:300%; font-family: Gabriola; position: absolute; top: 0px; margin: -10px 0px 0px 74px; color: #0000ff;">Best Táxi</p>
<a href="../Logout.php" class="sair_m_c" style="float: right; padding: 18px 18px 17px 18px; font-size:120%; margin: -7px 20px 0px 0px; text-decoration:none;">Sair</a>
<a href="../Index.php?p=home" class="sair_m_c" style="float: right; padding: 18px 12px 17px 12px; font-size:120%; margin: -7px 0px 0px 0px; text-decoration:none;">Menu</a>
</div>

<p style="float: right;position: absolute; top: 0px; margin: 210px 0px 0px 420px; font-size:140%; color: #555555;">Editar</p>
<div style=" width: 600px; height: 130px; margin: 180px auto 0px auto; padding: 20px 0px 0px 0px; text-align: center; border: 1px solid #d7d6d6; background: #ececec; border-radius:6px;">
<?php
$sql = mysql_query("SELECT * FROM carro_has_taxista WHERE id_carro_has_taxista = '".$_GET['cod']."'");
$ln = mysql_fetch_array($sql);
if($ln['taxista_idtaxista']==$_SESSION['idtaxista']){}else{echo "<script>location = '../minha_conta.php?p=veic'; </script>";}
?>
		<!--CADASTRO-->
        <div style=" width: 520px; margin: auto; text-align: left;">
		<form method="post" action="">
		<input type="text" name="idcod" style="display:none;" value="<?php echo $ln['id_carro_has_taxista'];?>"/>
		<p style="margin: 0px 0px 8px 0px;">
		<!--------------------------------------------------------------------------------------------------------------------------------->
        <label for="marca"><strong>Marca:</strong>
		<select name="marca" class="input-medium" id="marca">
		<option name="" selected="selected">Selecione</option>
		<?php
		$query = mysql_query( "select * from marca" );
		while( $marcas = mysql_fetch_array( $query ) ):?>
		<option value="<?php echo $marcas['idmarca'] ?>"><?php echo $marcas['nome'] ?></option>
		<?php endwhile; ?>
		</select>
        </label>
		<label for="modelo">
		<strong>Modelo:</strong> <select name="modelo" class="input-medium" id="modelo"></select>
		</label>		
		<!---------------------------------------------------------------------------------------------------------------------------------->
	<!--Ano-->
	<div style="float: right;position: absolute; top: 0px; margin: 260px 0px 0px 342px;">
    <strong>Ano:</strong>
	<input type="hidden" value="<?php $data = date("Y-m-d", mktime(date("H")-4, date("i"), date("s"), date("m"), date("d"), date("Y"), 0));?>">
	<?php $rest = substr($data, 0, -6); $rest = $rest + 1; $ano = "2000";?>
	<select name="ano">
	<?php  while($ano <= $rest){ 
	if($ano == $ln['ano']){?>
	<option value="<?php echo $ano;?>" selected><?php echo $ano; $ano ++;?></option>
	<?php }else{?>
	<option value="<?php echo $ano;?>"><?php echo $ano; $ano ++;?></option>
	<?php } ?>
	<?php } ?>
	</select>
	</div>
	<!--Fim Ano-->		
		</p>		
		<p style="margin: 0px 0px 8px 0px;">
		<!--input para receber somente "numeros"-->
		<strong>Renavam:</strong> <input type="text" name="renavam" placeholder="RENAVAM" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 100px; margin: 0px 10px 0px 0px;" maxlength="11" value="<?php echo $ln['renavam'];?>"/>
		<!--FIM input-->
		<strong>Placa:</strong><!--input para receber somente "letras"-->
<input type="text" name="placax" placeholder="XXX" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 65; for($cont=65; $cont<=122;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style=" width: 34px; text-align: center; text-transform: uppercase;"maxlength="3" value="<?php $placax = substr($ln['placa'], 0, -5); echo $placax;?>"/> - 
<!--FIM input-->
<!--input para receber somente "numeros"-->
<input type="text" name="placa0" placeholder="0000" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="margin: 0px 10px 0px 0px;width: 34px; text-align: center; " maxlength="4" value="<?php $placa0 = substr($ln['placa'], 4, 8); echo $placa0;?>"/>
<!--FIM input-->
	
	    <strong>Cor:</strong> <input type="text" name="cor" placeholder="COR" style="width: 130px; margin: 0px 0px 10px 2px;" value="<?php echo $ln['cor'];?>"/></p>
		<input type="hidden" name="acao" value="comentar"/>
		<input type="submit" value="Alterar" class="form-cad" style="width: 100px; height: 35px; margin: 0px 0px 0px 288px; border-radius:5px;"/>
		<a href="../minha_conta.php?p=veic" class="form-cancelar" style="padding: 6px 21px 8px 20px; background: #dedede; font-size: 97%;">Cancelar</a>
		</form>
		</div>
		
        <!--fim cadastro-->
		
</div>


<?php
if(isset($_POST['acao']) && $_POST['acao'] == 'comentar'){
$_POST['placax'] = strtoupper($_POST['placax']);
$sqlInserir = mysql_query("UPDATE carro_has_taxista SET carro_idcarro =  '".$_POST['modelo']."', taxista_idtaxista = '".$_SESSION['idtaxista']."', placa = '".$_POST['placax']."-".$_POST['placa0']."', renavam = '".$_POST['renavam']."', ano = '".$_POST['ano']."', cor = '".$_POST['cor']."' WHERE id_carro_has_taxista = '".$_POST['idcod']."'");

echo "<script> location = '../minha_conta.php?p=veic'; </script>";
}
?>
</body>
</html>