<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="img/2a.png">
<title>Best Táxi</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<!--Reveal modal-->
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>

<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script> 
    <script type="text/javascript">
    $(document).ready(function(){
         
        $("#marca").change(function(){
            marca = $(this).attr('value');
            $.ajax({
                type: "post",
                url: "pagtaxi/get_modelos.php",
                data: "marca="+marca+"",
                async: false,
                cache: false,
                datatype: "text",
                beforeSend: function(){
                    $("#modelo").html('<option value="" selected="selected">Aguarde...</option>');
                    $("#tipo").html('');
                },
                success: function(response){ 
                    x = response.split(",");
                    modelo = new Array();
                    for(i = 0; i < x.length; i++){
                        modelo[i] = x[i].split("|");
                    }
                    if(modelo.length > 0){
                        $("#modelo").html('').append('<option selected="selected">Selecione...</option>');
                        for(i = 0; i < modelo.length; i++) {
                            $("#modelo").append('<option value='+modelo[i][1]+'>'+modelo[i][0]+'</option>');
                        }   
                    }
                },
                error: function(){
                 
                }
            });
        });
    });
    </script>
	
<link rel="stylesheet" href="css/reveal.css">
<!--Fim Reveal modal-->
</head>

<body>
<p style="margin: 5px 0px 0px 40px; font-size:120%; color: #555555;">Associação</p>
<?php
$sqlT = mysql_query("SELECT * FROM taxista WHERE idtaxista = '".$_SESSION['idtaxista']."'");
$lnT = mysql_fetch_array($sqlT);
if($lnT['associacao_idassociacao']==0){}else{
?>
<a href="#" style="float: right; margin: -18px 2px 0px 0px;" class="novo" data-reveal-id="novo_veiculo" data-animation="fade">Deixar Associação</a><br/>
<?php } ?>

<div style="margin: 4px 0px 0px 30px; width: 600px; height: 270px; background: #eeeeee; border: 1px solid #cfcfcf;  border-radius:6px;">
<?php
if($lnT['associacao_idassociacao']==0){ ?>
<p style="margin: 90px auto 0px auto; width: 330px;">Você não está filiado a nenhuma associação, para se filiar a uma associação click no botão abaixo.</p>
<p style="margin: 20px auto 0px auto; width: 70px;"><a href="#" class="selecionar" data-reveal-id="selecionar" data-animation="fade">Filiar-se</a></p>
<?php }else{
?>
<div style="float: left; width: 170px; text-align: right; ">
<p style="margin: 20px 0px 8px 0px;"><strong>Nome:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Endereço:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Complemento:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Bairro:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Cep:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Cidade:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Cnpj:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Telefone:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>E-mail:</strong></p>
</div>

<div style="float: left; width: 424px; text-align: left; margin: 0px 0px 0px 6px;">
<?php
$sql = mysql_query("SELECT * FROM associacao WHERE idassociacao = '".$lnT['associacao_idassociacao']."'");
$ln = mysql_fetch_array($sql);
?>
<p style="margin: 20px 0px 8px 0px;"><?php echo $ln['nome'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo "Rua ".$ln['rua']."; Nº ".$ln['numero']."";?></p>
<p style="margin: 0px 0px 8px 0px;"><?php if(empty($ln['complemento'])){echo "-";}else{echo $ln['complemento'];}?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['bairro_ass'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['cep'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo "".$ln['cidade_ass']."-".$ln['estado_ass']."";?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['cnpj'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo "(".$ln['ddd_telefone_ass'].") ".$ln['telefone']."";?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['email'];?></p>
</div>
<?php } ?>
</div>

		<!--CADASTRO-->
		<div id="novo_veiculo" class="reveal-modal">
		<form method="post" action="">
		<div style=" width: 520px; text-align: center;">
		<p style="font-size:120%; color: #f42c2c; margin: 0px 0px 8px 0px;">Deseja realmente se desligar desta associação?</p>
		<input type="text" name="excluirV" style="display: none;" value="<?php echo $ln['id_carro_has_taxista'];?>"/>
		<input type="hidden" name="acao" value="comentar"/>
		<input type="submit" value="SIM" class="form-cad" style="width: 100px; height: 35px; margin: 0px 0px 0px 240px; border-radius:5px;"/>
		</div>		
		<a href="" class="close-reveal-modal"> x</a>
		</form>
		</div>
        <!--fim cadastro-->

		<!--FILIAR-->
		<div id="selecionar" class="reveal-modal">
		<form method="post" action="">
		<div style=" width: 520px; text-align: center;">
		<p style="font-size:120%; color: #555555; margin: 0px 0px 8px 0px;">Selecionar associação</p>

	<select name="associacao" style="width: 300px; height: 22px; margin: 0px 0px 10px 70px;">
				<?php
				$sqlForn = mysql_query("SELECT * FROM associacao order by nome ASC");
				while ($dadosForn = mysql_fetch_array($sqlForn)){ ?>
					<option value="<?php echo $dadosForn['idassociacao'];?>"><?php echo $dadosForn['nome'];?></option>
				<?php } ?>
	</select>
		
		<input type="hidden" name="acaoS" value="comentarS"/>
		<input type="submit" value="Concluir" class="form-cad" style="width: 100px; height: 35px; margin: 0px 0px 0px 0px; border-radius:5px;"/>
		</div>
		<a href="" class="close-reveal-modal"> x</a>
		</form>
		</div>
        <!--fim filiar-->		
		
<!--PHP Cadastro-->
<?php
if(isset($_POST['acao']) && $_POST['acao'] == 'comentar'){

$sql = mysql_query("UPDATE taxista SET associacao_idassociacao =  0, status_ass = 0 WHERE idtaxista = '".$_SESSION['idtaxista']."'");

echo "<script> location = 'minha_conta.php?p=asso'; </script>";
}
?>
<!--FIM PHP Cadastro-->
<!--PHP ASS-->
<?php
if(isset($_POST['acaoS']) && $_POST['acaoS'] == 'comentarS'){

$sql = mysql_query("UPDATE taxista SET associacao_idassociacao =  '".$_POST['associacao']."' WHERE idtaxista = '".$_SESSION['idtaxista']."'");

echo "<script> location = 'minha_conta.php?p=asso'; </script>";
}
?>
<!--FIM PHP Ass-->
</body>
</html>