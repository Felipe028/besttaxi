<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="img/2a.png">
<title>Best Táxi</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<!--Reveal modal-->
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<script type="text/javascript" src="js/cidades-estados-v0.2.js"></script>
<script>
$(document).ready(function() {
  $('#inputOculto').hide();
  $('#inputOcultoA').hide();
  $('#mySelect').change(function() {
    if ($('#mySelect').val() == 1) {
      $('#inputOculto').show();
	  $('#inputOcultoA').show();
    } else {
      $('#inputOculto').hide();
	  $('#inputOcultoA').hide();
    }
  });
});

$(document).ready(function() {
  $('#inputOculto2').hide();
  $('#mySelect2').change(function() {
    if ($('#mySelect2').val() == 1) {
      $('#inputOculto2').show();
    } else {
      $('#inputOculto2').hide();
    }
  });
});
</script>
<link rel="stylesheet" href="css/reveal.css">
<!--Fim Reveal modal-->
</head>

<body>
<p style="margin: 5px 0px 0px 40px; font-size:120%; color: #555555;">Viagens</p>
<a href="#" style="float: right; margin: -18px 2px 0px 0px;" class="novo" data-reveal-id="editar" data-animation="fade">Nova Viagem</a>
<?php
$sql = mysql_query("SELECT * FROM taxista WHERE idtaxista = '".$_SESSION['idtaxista']."'");
$ln = mysql_fetch_array($sql);
?>

<ul class="viagemtx">

<?php $sql = mysql_query("SELECT * FROM viagem WHERE taxista_idtaxista = '".$_SESSION["idtaxista"]."' ORDER BY data_postagem DESC, hora_postagem DESC LIMIT 20");
while($ln = mysql_fetch_array($sql)){
 ?>
<li><a href="Visualizar_tabela.php?idTabela=<?php echo $id;?>" title="Mais" data-reveal-id="<?echo $ln['idviagem'];?>" data-animation="fade">
<div style="width: 600px; height: 54px; background: #e4e4e4; -moz-border-radius:6px; -webkit-border-radius:6px; border: 1px solid #cfcfcf;">
<div style="width: 296px; float: left;">
<p style="margin: 10px 4px 0px 20px;">Data prevista para a saída: <?if($ln['data_prevista'] < $ln['data_postagem']){echo "Idefinida";}else{echo date("d-m-Y", strtotime($ln['data_prevista']));}?></p>
<p style="margin: 0px 4px 0px 20px;">Hora prevista para a saída: <?if($ln['hora_prevista'] == "24:00:00"){echo "Idefinida";}else{echo $ln['hora_prevista'];}?></p>
</div>
<div style="width: 196px; float: right; text-align: right;">
<p style="font-size: 80%; margin: 0px 4px 0px 0px;">Postado em: <?echo date("d-m-Y", strtotime($ln['data_postagem']));?> as <?echo $ln['hora_postagem'];?></p>
<p style="font-size: 140%; margin: 0px 4px 0px 0px;">Vagas: <?echo $ln['vagas'];?></p>
<p style="font-size: 80%; margin: 0px 4px 0px 0px;">Status: <?if($ln['status_2']==0){?><strong style="color: #15df10;">Ativo</strong><?}else{?><strong style="color: #df1010;">Inativo</strong><?}?></p>
</div>
</div>
</a></li>


<div id="<?echo $ln['idviagem'];?>" class="reveal-modal">
	<form method="post" action="pagtaxi/ediviagem.php">
	<div style="border: none; border-top:2px solid #a6a6a6; margin: 0px 0px 0px -12px; -moz-border-radius:6px; -webkit-border-radius:6px; background: #e7e7e7; padding: 0px 0px 5px 0px;">
	<p style="font-size: 110%; margin: 5px 0px 0px 5px;"><strong>Cidade origem:</strong> <?echo "".$ln['cidade_origem']."-".$ln['estado_origem']."";?></p>
	<p style="font-size: 110%; margin: 2px 0px 0px 5px;"><strong>Cidade destino:</strong> <?echo "".$ln['cidade_destino']."-".$ln['estado_destino']."";?></p>
	</div>
	<div style="border: none; border-top:2px solid #a6a6a6; margin: 10px 0px 0px -12px; -moz-border-radius:6px; -webkit-border-radius:6px; background: #e7e7e7; height: 63px;">

    <div style="float: left; width: 300px;">
	<p style="font-size: 110%; margin: 5px 0px 0px 5px;">Data prevista: <?if($ln['status_2']==0){?><input type="date" name="data_prevA" style="-moz-border-radius:6px; -webkit-border-radius:6px; margin: 0px 0px 0px 2px;" value="<?echo $ln['data_prevista'];?>"/><?}else{echo $ln['data_prevista'];}?></p>
	<p style="font-size: 110%; margin: 5px 0px 0px 5px;">Hora prevista: <?if($ln['status_2']==0){?><input type="time" name="hora_prevA" style="-moz-border-radius:6px; -webkit-border-radius:6px; margin: 0px 0px 5px 0px;" value="<?echo $ln['hora_prevista'];?>"/><?}else{echo $ln['hora_prevista'];}?></p>
    </div>
	
	<input type="date" name="datpost" value="<?echo $ln['data_postagem'];?>" style="display: none;"/>
	<input type="text" name="cod" value="<?echo $ln['idviagem'];?>" style="display: none;"/>
	
	<div style="float: right; width: 200px; text-align: right; height: 63px;">
	<p style="font-size: 140%;  margin: 16px 10px 0px 0px;">Vagas
	<!--input para receber somente "numeros"-->
    <?if($ln['status_2']==0){?><input type="text" name="num" placeholder="Nº" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="-moz-border-radius:6px; -webkit-border-radius:6px; font-size: 100%; text-align: center; width: 40px; " maxlength="1" value="<?echo $ln['vagas'];?>"/>
    <!--FIM input--><?}else{echo $ln['vagas'];}?></p>
	</div>
	
	</div>
	
	<?if($ln['status_2']==0){?>
	<a href="#" style="text-decoration:none; font-size: 84%; font-family: Arial;margin: 10px 0px 0px 296px; float: left; background: #e85c41; -moz-border-radius:5px; -webkit-border-radius:5px;" class="can<?echo $ln[''];?>" data-reveal-id="can<?echo $ln['idviagem'];?>" data-animation="fade">
	<div style="width: 110px; height: 25px; padding: 10px 0px 0px 0px; color:#ffffff; text-align: center;">
	Retirar anúncio
	</div></a>

	<input type="submit" value="Concluir" class="confirmar" style="margin: 10px 0px 0px 3px; width: 110px; height: 35px; -moz-border-radius:5px; -webkit-border-radius:5px;"/>
		
	<?}else{}?>
	<a href="" class="close-reveal-modal"> x</a>
	
	</form>
</div>
<!--CANCELAR-->
<div id="can<?echo $ln['idviagem'];?>" class="reveal-modal" style="height: 180px; text-align: center;">
<form method="post" action="pagtaxi/retiraranuncio.php">
<p style="font-size:120%; color: #f42c2c; margin: 50px 0px 0px 0px;">Deseja realmente retirar esse anúncio?</p>
<p style="font-size:120%; color: #f42c2c; margin: 0px 0px 8px 0px;">Esse processo não pode ser revertido.</p>
<input type="text" value="<?echo $ln['idviagem'];?>" name="retA" style="display: none;"/>
<input type="submit" value="CONFIRMAR" class="form-cad" style="width: 100px; height: 35px; margin: 0px 0px 0px 200px; border-radius:5px;"/>
</form>
<a href="" class="close-reveal-modal"> x</a>
</div>
<!--FIM CANCELAR-->
<? } ?>
</ul>

		<!--NOVA VIAGEM-->
		<div id="editar" class="reveal-modal">
		<div style=" width: 520px; text-align: center;">
    <form method="post" action="pagtaxi/novaviagem.php">
	<p style="margin:0; font-size:120%;">Preencha os campos abaixo com as informações da viagem.</p>
	<p style="margin: 10px 0px 0px 0px; font-size:120%; width: 300px; text-align: left; color: #6f6f6f;">Origem</p>
	<fieldset style="border: none; border-top:2px solid #a6a6a6; margin: 0px 0px 6px -12px;  -moz-border-radius:6px; -webkit-border-radius:6px; background: #e7e7e7;">
		<label>Estado</label>: <select id="estado" name="estado"></select> 
		<label>Cidade</label>: <select id="cidade" name="cidade"></select> 
	</fieldset>
	<p style="margin: 10px 0px 0px 0px; font-size:120%; width: 300px; text-align: left; color: #6f6f6f;">Destino</p>
	<fieldset style="border: none; border-top:2px solid #a6a6a6; margin: 0px 0px 6px -12px; -moz-border-radius:6px; -webkit-border-radius:6px; background: #e7e7e7;">
		<label>Estado</label>: <select id="estado1" name="estado1"></select> 
		<label>Cidade</label>: <select id="cidade1" name="cidade1"></select> 
	</fieldset>

	<p style="margin: 10px 0px 0px 0px; font-size:120%; width: 300px; text-align: left; color: #6f6f6f;">Data Prevista</p>
	<div style="border: none; border-top:2px solid #a6a6a6; margin: 0px 0px 0px -12px; -moz-border-radius:6px; -webkit-border-radius:6px; background: #e7e7e7;">
	<p style="margin: 5px 0px 5px 0px;">Você tem data prevista para a viagem?
	<select id="mySelect" name="pagamento" style="height: 23px; -moz-border-radius:6px; -webkit-border-radius:6px;  margin: 0px 0px 5px 0px;">
	<option value="1">SIM</option>
	<option value="2" selected>NÃO</option>
	</select></p>
	<div style="display:none;" id="inputOculto">
	Informe a data prevista para a viagem.
	<input type="date" name="data_prev" style="-moz-border-radius:6px; -webkit-border-radius:6px; margin: 0px 0px 5px 0px;"/>
	</div>
	</div>

	<div style="display:none;" id="inputOcultoA">
	<p style="margin: 10px 0px 0px 0px; font-size:120%; width: 300px; text-align: left; color: #6f6f6f;">Hora Prevista</p>	
	<div style="border: none; border-top:2px solid #a6a6a6; margin: 0px 0px 0px -12px; -moz-border-radius:6px; -webkit-border-radius:6px; background: #e7e7e7;">
	<p style="margin: 5px 0px 5px 0px;">Você tem hora prevista para a viagem?
	<select id="mySelect2" name="pagamento" style="height: 23px; -moz-border-radius:6px; -webkit-border-radius:6px; margin: 0px 0px 5px 0px;">
	<option value="1">SIM</option>
	<option value="2" selected>NÃO</option>
	</select></p>
	<div style="display:none;" id="inputOculto2">
	Informe a hora prevista para a viagem.
	<input type="time" name="hora_prev" style="-moz-border-radius:6px; -webkit-border-radius:6px; margin: 0px 0px 5px 0px;"/>
	</div>
	</div></div>

	<p style="margin: 10px 0px 0px 0px; font-size:120%; width: 300px; text-align: left; color: #6f6f6f;">Nº de Vagas</p>	
	<div style="border: none; border-top:2px solid #a6a6a6; margin: 0px 0px 0px -12px; -moz-border-radius:6px; -webkit-border-radius:6px; background: #e7e7e7;">
	<p style="margin: 5px 0px 5px 0px;">Informe a quantidade de vagas disponíveis no veículo.
	<!--input para receber somente "numeros"-->
    <input type="text" name="vagas" placeholder="Nº" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 20px; margin: 0px 0px 5px 0px; text-align: center;" maxlength="1"/>
    <!--FIM input--></p>
	</div>	
	
	<input type="submit" value="Concluir" class="confirmar" style="width: 100px; height: 35px;  margin: 10px 0px 0px 420px; -moz-border-radius:5px; -webkit-border-radius:5px;"/>
	<a href="" class="close-reveal-modal"> x</a>
</form>
		</div>
     
</body>
</html>