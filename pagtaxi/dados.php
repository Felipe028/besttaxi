<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="img/2a.png">
<title>Best Táxi</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<!--Reveal modal-->
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<script type="text/javascript" src="js/cidades-estados-v0.2.js"></script> 
<script type="text/javascript">
    window.onload = function() {
        new dgCidadesEstados( 
            document.getElementById('estado'), 
            document.getElementById('cidade'), 
            true
        );
    }
</script>
<link rel="stylesheet" href="css/reveal.css">
<!--Fim Reveal modal-->
</head>

<body>
<p style="margin: 5px 0px 0px 40px; font-size:120%; color: #555555;">Meus dados</p>
<a href="#" style="float: right; margin: -18px 2px 0px 0px;" class="novo" data-reveal-id="editar" data-animation="fade">Editar dados</a>
<?php
$sql = mysql_query("SELECT * FROM taxista WHERE idtaxista = '".$_SESSION['idtaxista']."'");
$ln = mysql_fetch_array($sql);
?>
<div style="margin: 22px 0px 0px 30px; width: 600px; height: 372px; background: #eeeeee; border: 1px solid #cfcfcf;  border-radius:6px;">

<div style="float: left; width: 170px; text-align: right; ">
<p style="margin: 20px 0px 8px 0px;"><strong>Nome:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Endereço:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Complemento:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Bairro:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Cep:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Cidade:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>RG:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>CPF:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Data/Nascimento:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>CNH:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Telefone:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>E-mail:</strong></p>
<p style="margin: 0px 0px 8px 0px;"><strong>Situação da sua conta:</strong></p>
</div>

<div style="float: left; width: 424px; text-align: left; margin: 0px 0px 0px 6px;">
<p style="margin: 20px 0px 8px 0px;"><?php echo $ln['nome'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo "Rua ".$ln['rua']."; Nº ".$ln['numero']."";?></p>
<p style="margin: 0px 0px 8px 0px;"><?php if(empty($ln['complemento'])){echo "-";}else{echo $ln['complemento'];}?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['bairro'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['cep'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo "".$ln['cidade_taxi']."-".$ln['estado_taxi']."";?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['rg'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['cpf'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo date("d-m-Y", strtotime($ln['data_nasc']));?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['cnh'];?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo "(".$ln['ddd'].") ".$ln['telefone']."";?></p>
<p style="margin: 0px 0px 8px 0px;"><?php echo $ln['email'];?></p>
<p style="margin: 0px 0px 8px 0px;">
<?php if($ln['status_ass']==0){ echo "Aguardando aprovação"; }?>
<?php if($ln['status_ass']==1){ echo "Regular";}?>
<?php if($ln['status_ass']==2){ echo "Supenso por indisciplina";}?>
<?php if($ln['status_ass']==3){ echo "Supenso por falta de pagamento";}?>
<?php if($ln['status_ass']==4){ echo "Solicitação rejeitada";}?>
</p>
</div>
</div>


		<!--FILIAR-->
		<div id="editar" class="reveal-modal">
		<form method="post" action="">
		<div style=" width: 520px; text-align: center;">
		<p style="font-size:120%; color: #555555; margin: 0px 0px 8px 0px;">Alterar Dados</p>

	Nome: <input type="text" name="nome" placeholder="NOME" style="width: 438px; margin: 0px 0px 10px 2px;" value="<?php echo $ln['nome'];?>"/><br/>
	<!--input para receber somente "numeros"-->
	<!--input para receber somente "numeros"-->
    CNH: <input type="text" name="cnh" placeholder="CNH" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 100px; margin: 0px 37px 10px 8px;" maxlength="10" value="<?php echo $ln['cnh'];?>"/>
    <!--FIM input-->
    RG: <input type="text" name="rg" placeholder="RG" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 80px; margin: 0px 37px 10px 0px;" maxlength="8" value="<?php echo $ln['rg'];?>"/>
    <!--FIM input-->
	<!--input para receber somente "numeros"-->
    CPF: <input type="text" name="cpf" placeholder="CPF" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 100px; margin: 0px 0px 10px 0px;" maxlength="11" value="<?php echo $ln['cpf'];?>"/><br/>
    <!--FIM input-->
	Rua: <input type="text" name="rua" placeholder="RUA" style="width: 344px; margin: 0px 8px 10px 16px;" value="<?php echo $ln['rua'];?>"/>
	<!--input para receber somente "numeros"-->
    Nº: <input type="text" name="numero" placeholder="Nº" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 53px;" maxlength="6" value="<?php echo $ln['numero'];?>"/><br/>
    <!--FIM input-->
	Complemento: <input type="text" name="complemento" placeholder="COMPLEMENTO" style="width: 390px; margin: 0px 0px 10px 0px;" value="<?php echo $ln['complemento'];?>"/><br/>
	Bairro: <input type="text" name="bairro" placeholder="BAIRRO" style="width: 315px; margin: 0px 6px 10px 3px;" value="<?php echo $ln['bairro'];?>"/>
	<!--input para receber somente "numeros"-->
    CEP: <input type="text" name="cep" placeholder="CEP" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 70px;" maxlength="8" value="<?php echo $ln['cep'];?>"/><br/>
    <!--FIM input-->
	
	<fieldset style="border: none; margin: 0px 0px 2px -12px;">
		<label>Estado</label>: <select id="estado" name="estado"></select> 
		<label>Cidade</label>: <select id="cidade" name="cidade"></select> 
	</fieldset>
	
	Data de nascimento: <input type="date" name="nascimento" style="width: 124px; margin: 2px 11px 10px 0px;" value="<?php echo $ln['data_nasc'];?>"/>
	Telefone: <!--input para receber somente "numeros"-->
    <input type="text" name="ddd" placeholder="DDD" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 30px; text-align: center" maxlength="2" value="<?php echo $ln['ddd'];?>"/>
    <!--FIM input-->
	<!--input para receber somente "numeros"-->
     - <input type="text" name="telefone" placeholder="TELEFONE" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 100px;" maxlength="9" value="<?php echo $ln['telefone'];?>"/><br/>
    <!--FIM input-->
	E-mail: <input type="text" name="email" placeholder="E-MAIL" style="width: 232px; margin: 0px 0px 20px 0px;" value="<?php echo $ln['email'];?>"/>
	Senha: <input type="text" name="senha" placeholder="SENHA" style="width: 150px;" value="<?php echo $ln['senha'];?>"/>
		
		
		<input type="hidden" name="acao" value="comentar"/>
		<input type="submit" value="Concluir" class="form-cad" style="width: 100px; height: 35px; margin: 0px 0px 0px 390px; border-radius:5px;"/>
		</div>
		<a href="" class="close-reveal-modal"> x</a>
		</form>
		</div>
        <!--fim filiar-->		
<!--PHP Alterar-->
<?php
if(isset($_POST['acao']) && $_POST['acao'] == 'comentar'){

$sql = mysql_query("UPDATE taxista SET nome = '".$_POST['nome']."', rua = '".$_POST['rua']."', numero = '".$_POST['numero']."', complemento = '".$_POST['complemento']."', bairro = '".$_POST['bairro']."', cidade_taxi = '".$_POST['cidade']."', estado_taxi = '".$_POST['estado']."', cep = '".$_POST['cep']."', data_nasc = '".$_POST['nascimento']."', rg = '".$_POST['rg']."', cpf = '".$_POST['cpf']."', cnh = '".$_POST['cnh']."', ddd = '".$_POST['ddd']."', telefone = '".$_POST['telefone']."', email = '".$_POST['email']."', senha = '".$_POST['senha']."' WHERE idtaxista = '".$_SESSION['idtaxista']."'");

echo "<script> location = 'minha_conta.php?p=dado'; </script>";
}
?>
<!--FIM PHP Alterar-->		

</body>
</html>