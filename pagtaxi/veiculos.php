<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br" xml:lang="pt-br">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="img/2a.png">
<title>Best Táxi</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<!--Reveal modal-->
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>

<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script> 
    <script type="text/javascript">
    $(document).ready(function(){
         
        $("#marca").change(function(){
            marca = $(this).attr('value');
            $.ajax({
                type: "post",
                url: "pagtaxi/get_modelos.php",
                data: "marca="+marca+"",
                async: false,
                cache: false,
                datatype: "text",
                beforeSend: function(){
                    $("#modelo").html('<option value="" selected="selected">Aguarde...</option>');
                    $("#tipo").html('');
                },
                success: function(response){ 
                    x = response.split(",");
                    modelo = new Array();
                    for(i = 0; i < x.length; i++){
                        modelo[i] = x[i].split("|");
                    }
                    if(modelo.length > 0){
                        $("#modelo").html('').append('<option selected="selected">Selecione...</option>');
                        for(i = 0; i < modelo.length; i++) {
                            $("#modelo").append('<option value='+modelo[i][1]+'>'+modelo[i][0]+'</option>');
                        }   
                    }
                },
                error: function(){
                 
                }
            });
        });
    });
    </script>
	
<link rel="stylesheet" href="css/reveal.css">
<!--Fim Reveal modal-->
</head>

<body>
<p style="margin: 5px 0px 0px 40px; font-size:120%; color: #555555;">Veículos</p>
<a href="#" style="float: right; margin: -18px 2px 0px 0px;" class="novo" data-reveal-id="novo_veiculo" data-animation="fade">Cadastrar Veículo</a><br/>

<table id="tabela" >
    <tbody>
	    <tr class="tabela-new-venda">
		    <th style="width: 150px;">Modelo</th>
			<th style="width: 130px;">Marca</th>
			<th style="width: 80px;">Ano</th>
			<th style="width: 80px;">Cor</th>
			<th style="width: 90px;">Placa</th>
			<th style="width: 100px;">Renavam</th>
			<th style="width: 51px;"></th>
		</tr>
		
		<?php
		$sql = mysql_query("SELECT * FROM carro_has_taxista WHERE taxista_idtaxista = '".$_SESSION['idtaxista']."'");
		while($ln = mysql_fetch_array($sql)){
			$sqlCar = mysql_query("SELECT * FROM carro WHERE idcarro = '".$ln['carro_idcarro']."'");
			$lnCar = mysql_fetch_array($sqlCar);
			$sqlMar = mysql_query("SELECT * FROM marca WHERE idmarca = '".$lnCar['marca_idmarca']."'");
			$lnMar = mysql_fetch_array($sqlMar);
			?>
		<tr style="height: 38px;">
		    <td style="text-align: center;"><?echo $lnCar['nome'];?></td>
			<td style="text-align: center;"><?echo $lnMar['nome'];?></td>
			<td style="text-align: center;"><?echo $ln['ano'];?></td>
			<td style="text-align: center;"><?echo $ln['cor'];?></td>
			<td style="text-align: center;"><?echo $ln['placa'];?></td>
			<td style="text-align: center;"><?echo $ln['renavam'];?></td>
			<td style="text-align: center;">
			<a href="pagtaxi/alterar_veiculo.php?cod=<?echo $ln['id_carro_has_taxista'];?>"><img src="img/bt-editar.png" class="editar-img" title="Editar"/></a>
			<a href="#" data-reveal-id="exc<?php echo $ln['id_carro_has_taxista'];?>" data-animation="fade">
			<img src="img/bt-excluir.png" class="excluir-img" title="Excluir"/>
			</a>
			</td>
		</tr>

		<!--Excluir-->
		<div id="exc<?php echo $ln['id_carro_has_taxista'];?>" class="reveal-modal">
		<form method="post" action="">
		<div style=" width: 520px; text-align: center;">		
        <input type="hidden" value="<?php $hora = date("H:i:s", mktime(date("H")-4, date("i"), date("s"), date("m"), date("d"), date("Y"), 0));?>">
		<?php
		$res = substr($hora, 0, -6);
		$c = (($ln['id_carro_has_taxista'] + 7) * 3)-$ln['id_carro_has_taxista'] + $res;?>
		<p style="font-size:120%; color: #f42c2c; margin: 0px 0px 8px 0px;">Deseja realmente excluir esse veículo?</p>
		<input type="text" name="excluirV" style="display: none;" value="<?php echo $ln['id_carro_has_taxista'];?>"/>
		<input type="hidden" name="acaoExc" value="comentarExc"/>
		<input type="submit" value="SIM" class="form-cad" style="width: 100px; height: 35px; margin: 0px 0px 0px 180px; border-radius:5px;"/>
		</div>
		<a href="" class="close-reveal-modal"> x</a>
		</form>		
		</div>
        <!--fim excluir-->		
<?php } ?>
	</tbody>
</table>

		<!--CADASTRO-->
		<div id="novo_veiculo" class="reveal-modal">
		<form method="post" action="">
		<div style=" width: 520px; margin: auto; text-align: left;">
		<p style="margin: 0px 0px 8px 190px; font-size:120%; color: #555555;">Cadastrar Veículo</p>
		<p style="margin: 0px 0px 8px 0px;">
		<!--------------------------------------------------------------------------------------------------------------------------------->
        <label for="marca"><strong>Marca:</strong>
		<select name="marca" class="input-medium" id="marca">
		<option name="" selected="selected">Selecione</option>
		<?php
		$query = mysql_query( "select * from marca" );
		while( $marcas = mysql_fetch_array( $query ) ): ?>
		<option value="<?php echo $marcas['idmarca'] ?>"><?php echo $marcas['nome'] ?></option>
		<?php endwhile; ?>
		</select>
        </label>
		<label for="modelo">
		<strong>Modelo:</strong> <select name="modelo" class="input-medium" id="modelo"></select>
		</label>		
		<!---------------------------------------------------------------------------------------------------------------------------------->
	<!--Ano-->
	<div style="float: right;position: absolute; top: 0px; margin: 60px 0px 0px 342px;">
    <strong>Ano:</strong>
	<input type="hidden" value="<?php $data = date("Y-m-d", mktime(date("H")-4, date("i"), date("s"), date("m"), date("d"), date("Y"), 0));?>">
	<?php $rest = substr($data, 0, -6); $rest = $rest + 1; $ano = "2000";?>
	<select name="ano">
	<?php  while($ano <= $rest){ ?>
	<option value="<?php echo $ano;?>"><?php echo $ano; $ano ++;?></option>
	<?php } ?>
	</select>
	</div>
	<!--Fim Ano-->		
		</p>		
		<p style="margin: 0px 0px 8px 0px;">
		<!--input para receber somente "numeros"-->
		<strong>Renavam:</strong> <input type="text" name="renavam" placeholder="RENAVAM" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 100px; margin: 0px 10px 0px 0px;" maxlength="11"/>
		<!--FIM input-->
		<strong>Placa:</strong><!--input para receber somente "letras"-->
<input type="text" name="placax" placeholder="XXX" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 65; for($cont=65; $cont<=122;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style=" width: 34px; text-align: center; text-transform: uppercase;"maxlength="3"/> - 
<!--FIM input-->
<!--input para receber somente "numeros"-->
<input type="text" name="placa0" placeholder="0000" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="margin: 0px 10px 0px 0px;width: 34px; text-align: center; " maxlength="4"/>
<!--FIM input-->
	
	    <strong>Cor:</strong> <input type="text" name="cor" placeholder="COR" style="width: 130px; margin: 0px 0px 10px 2px;"/></p>
	    
		<input type="hidden" name="acaoCad" value="comentarCad"/>
		<input type="submit" value="Cadastrar" class="form-cad" style="width: 100px; height: 35px; margin: 0px 0px 0px 288px; border-radius:5px;"/>
		<input  type="reset" value="Limpar" class="form-cad" style="width: 100px; height: 35px; border-radius:5px;"/>
		
		</div>
				<a href="" class="close-reveal-modal"> x</a>
		</div>

		</form>
        <!--fim cadastro-->
		
<!--PHP Cadastro-->
<?php
if(isset($_POST['acaoCad']) && $_POST['acaoCad'] == 'comentarCad'){
$_POST['placax'] = strtoupper($_POST['placax']);
$sqlInserir = mysql_query("INSERT INTO  carro_has_taxista (carro_idcarro, taxista_idtaxista, placa, renavam, ano, cor) values ('".$_POST['modelo']."', '".$_SESSION['idtaxista']."', '".$_POST['placax']."-".$_POST['placa0']."', '".$_POST['renavam']."', '".$_POST['ano']."', '".$_POST['cor']."')");

echo "<script> alert( 'Cadastro efetuado com sucesso!' ); location = 'minha_conta.php?p=veic'; </script>";
}
?>
<!--FIM PHP Cadastro-->
<!--PHP Excluir-->
<?php
if(isset($_POST['acaoExc']) && $_POST['acaoExc'] == 'comentarExc'){
	
$sql = mysql_query("DELETE FROM carro_has_taxista WHERE id_carro_has_taxista = '".$_POST['excluirV']."'");

echo "<script>location = 'minha_conta.php?p=veic'; </script>";
}
?>
<!--FIM PHP Excluir-->

</body>
</html>